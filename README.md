# Description

Learn more on [opensearch-benchmark](https://github.com/opensearch-project/opensearch-benchmark)

# Prepare environment

## Install opensearch-benchmark

```bash
pip3 install opensearch-benchmark
```

## Get Opensearch cluster CA cert

1. Download opensearch certificate from server
```bash
scp <server_user>@<server_ip>:/opt/opensearch-1.2.0/config/root-ca.pem ./
```
2. Add hosts entry to Opensearch host
```bash
echo '<local_server_ip> node-0.example.com' >> /etc/hosts
```

# Execute tests

```bash
opensearch-benchmark execute_test \
    --workload-path=./struct \
	--pipeline=benchmark-only \
	--target-hosts=node-0.example.com:9200 \
	--client-options=timeout:60,use_ssl:true,verify_certs:false,basic_auth_user:'admin',basic_auth_password:'admin',ca_certs:'root-ca.pem' \
    --exclude-tasks 'cluster-health'
```
